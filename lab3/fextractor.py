from __future__ import division
import os
import argparse

#CSF1819: 
    #Here you should add more features to the feature vector (features=[]) representing a cell trace

    #Function extract receives as input two sequences:
    #    times: timestamp of each cell
    #    sizes: direction of each cell (-1 / +1)

    #As of now, the only feature being used to distinguish between page loads is the total
    # amount of cells in each cell sequence and is given by len(times).

    # Shall some feature be missing due to impossibility of its calculation, 
    #please replace its value with "X". It will be replaced later.

def extract(times, sizes):
    features = []
#-------------------------------------------------
#Total amount of cells in sequence
  
    t = times[-1]               
    tempo = t
    x = len(times)/t    
    total = x
#----------------------------------------------------
    i = 0
    j = 0
    for size in sizes:
        if size == -1:
            i+=1                
        else:
            j+=1                
    downloadPercent =(i / (i + j)) * 100  
    uploadPercent = (j / (i + j)) * 100

    downloadSec = i /t                    
    uploadSec = j / t                  
    
    totalDownload = i
    totalUpload = j
    
    diferenca = i - j 
    diferencaPercent = downloadPercent / uploadPercent
#----------------------------------------------------
#    interval= []
#    for time in range (0,len(times)-1):
#        x = times[time+1]-times[time]
#        interval.append(x)
#
#    y1 = sum(interval) / len(interval)
#---------------------------------------------------
#    interval1= []
#    prev = 0
#    for i in range(1,len(sizes)):
#        if(sizes[i] == -1):
#            x = times[i]-times[prev]
#            interval1.append(x)
#            prev = i
#
#    media_d = sum(interval1) / len(interval1)
#---------------------------------------------------
#    interval1= []
#    prev = 0
#    for i in range(1,len(sizes)):
#        if(sizes[i] == 1):
#            x = times[i]-times[prev]
#            interval1.append(x)
#            prev = i
#
#    media_u = sum(interval1) / len(interval1)
#----------------------------------------------------
#    array = []
#    for i in range(0, len(sizes)-1):
#        if sizes[i] == 1:
#            if sizes[i + 1] == -1:
#                x = times[i+1] - times[i]
#                array.append(x)
#    if len(array) == 0:
#        media_d_u = "X"
#    else:
#        media_d_u = sum(array) / len(array)
#--------------------------------------------------
    seq_max_download = 1
    seq_max_upload = 1 
    count_max_d = 0
    count_max_u = 0
    seq = 1

    first=1
    second=0
    tempo_total_download = -1

    for i in range(1,len(sizes)):
        if(sizes [i] == sizes [i-1]) and sizes[i] == -1:
            seq+=1

            if first == 1:
                first = 0
                second = 1
                tempo_inicial = times[i-1]
            
        else:
            if seq > 5:
                count_max_d+=1
            if seq > seq_max_download:
                seq_max_download = seq

            if second == 1:
                tempo_final = times[i-1]
                tempo_total_download = tempo_total_download + (tempo_final - tempo_inicial)
                first = 1
                second = 0

            seq = 1
    
    seq = 1
    for i in range(1,len(sizes)):
        if(sizes [i] == sizes [i-1]) and sizes[i] == 1:
            seq+=1
        else:
            if seq > 5:
                count_max_u+=1
            if seq > seq_max_upload:
                seq_max_upload = seq
            seq = 1
#-------------------------------------------------    
    max_download_t = 1
    max_upload_t = 1
    count_download = 0
    count_upload = 0
    seq = 1

    for i in range(1,len(sizes)):
        if(sizes [i] == sizes [i-1]) and sizes[i] == -1 and times[i] == times[i-1]:
            seq+=1
        else:
            if seq > 1:
                count_download +=1
            if seq > max_download_t:
                max_download_t = seq
            seq = 1

    seq = 1
    for i in range(1,len(sizes)):
        if(sizes [i] == sizes [i-1]) and sizes[i] == 1 and times[i] == times[i-1]:
            seq+=1
        else:
            if seq > 1:
                count_upload+=1
            if seq > max_upload_t:
                max_upload_t = seq
            seq = 1
#--------------------------------------------------
#    seq = 0
#    max_ack = 0
#    for i in range(2,len(sizes)):
#        if(sizes [i] == -1 and sizes[i] != sizes [i-1]) and sizes[i] == sizes[i-2]:
#            seq+=1
#        else:
#            if seq > max_ack:
#                max_ack= seq
#            
#            seq = 0
#--------------------------------------------------
#    seq = 1
#    seq_time = 1
#    for i in range(1,len(times)):
#        if times [i] == times[i-1]:
#            seq+=1
#        else:
#            if seq > 1:
#                seq_time +=1
#            seq = 1
#--------------------------------------------------
    max_download = 0 
    primeiro = 0
    segundo = 0
    flag=0
    for i in range (1, len(sizes)):
        if (flag == 0 and sizes[i]== -1):
            primeiro = times[i]
            flag = 1
        else:
            if flag == 1 and sizes[i] == 1:
                segundo = times[i-1]
                tempo_download = (segundo - primeiro) *100
                if tempo_download > max_download:
                    max_download = tempo_download
                flag = 0
    if max_download == "0":
        max_download = "X"
#--------------------------------------------------
    seq_max_download_1 = 1
    seq_max_upload_1 = 1
    count_max_d_1 = 0
    count_max_u_1 = 0
    seq = 1
    first = 1
    second = 0
    tempo_total_download = -1
    media_tempo_burst_array = []
    first_3_bursts_size_d = 0
    contador = 3

    for i in range(1,len(sizes)-1):
        if(sizes [i] == sizes [i-1]) and sizes[i] == -1:
            seq+=1
                
            if first == 1:
                first = 0
                second = 1
                tempo_inicial = times[i-1]


        elif sizes[i-1] == sizes[i+1] and sizes[i-1]== -1:
            seq+=1
           
        else:
            if seq > 5:
                count_max_d_1+=1
                if contador != 0:
                    first_3_bursts_size_d += seq
                    contador -=1


            if seq > seq_max_download_1:
                seq_max_download_1 = seq

            if second == 1:
                tempo_final = times[i-1]
                tempo_decorrido = tempo_final - tempo_inicial
                tempo_total_download = tempo_total_download + tempo_decorrido
                media_tempo_burst_array.append(tempo_decorrido)

            seq = 1
    
    if len(media_tempo_burst_array) == 0:
        media_tempo_burst_d = "X"
    else:
        media_tempo_burst_d = sum(media_tempo_burst_array) / len(media_tempo_burst_array)

    seq = 1
    first = 1
    second = 0
    contador = 3   
    tempo_total_upload = -1
    media_tempo_burst_array = []
    first_3_bursts_size_u = 0

    for i in range(1,len(sizes)-1):
        if(sizes [i] == sizes [i-1]) and sizes[i] == 1:
            seq+=1


            if first == 1:
                first = 0
                second = 1
                tempo_inicial = times[i-1]


        elif sizes[i-1] == sizes[i+1] and sizes[i-1] == 1:
            seq+=1

        else:
            if seq > 5:
                count_max_u_1+=1

                if contador != 0:
                    first_3_bursts_size_u += seq
                    contador -=1

            if seq > seq_max_upload_1:
                seq_max_upload_1 = seq
            
            if second == 1:
                tempo_final = times[i-1]
                tempo_decorrido = tempo_final - tempo_inicial
                tempo_total_upload += tempo_decorrido
                media_tempo_burst_array.append(tempo_decorrido)
         
            seq = 1


    if len(media_tempo_burst_array) == 0:
        media_tempo_burst_u = "X"
    else:
        media_tempo_burst_u = sum(media_tempo_burst_array) / len(media_tempo_burst_array)

    
#---------------------------------------------------
    counter_primeiros_d = 1
    counter_primeiros_u = 1
    
    for i in range (0, 200):
        try:
            if sizes[i] == -1:
                counter_primeiros_d +=1
            elif sizes[i] == 1:
                counter_primeiros_u +=1
         
        except Exception as erro:
            pass
    
    i = counter_primeiros_d
    j = counter_primeiros_u

    downloadPercent_200 =(i / (i + j)) * 100
    uploadPercent_200 = (j / (i + j)) * 100
    
    try:
        t = times[150]
    except Exception as erro1:
        t = 1

    downloadSec_200 = i /t
    uploadSec_200 = j / t

    totalDownload_200 = i
    totalUpload_200 = j



#---------------------------------------------------
    counter_tempo_repetido = 0
    for i in range(0,len(times) -1):
        if times[i] == times[i+1]:
          counter_tempo_repetido+=1



    features.append(totalDownload)                  #quantidade total de downloads da pagina (1 e 5) 10
    features.append(totalUpload)                    #quantidade total de upload da pagina (1 e 5) 10
    
    #features.append(downloadPercent)                #percentagem de downloads da pagina(6)
    #features.append(uploadPercent)                  #percentagem de uploads da pagina
    
    #features.append(diferenca)                      #diferenca entre downloads e uploads
    #features.append(diferencaPercent)               #diferenca entre percentagens de download e upload
    
    features.append(downloadSec)                    #download por segundo (1 e 5) 10
    features.append(uploadSec)                      #upload por segundo (1 e 5) 10
    
    #features.append(y1)                             #media do intervalo de tempo entre cada captura 
    
    features.append(seq_max_download)               #sequencia maxima de downloads realizados (1) 10
    features.append(seq_max_upload)                 #sequencia maxima de uploads realizados (1) 10
    
    features.append(seq_max_download_1)             #sequencia maxima de download, aceita  um "1" a meio (5) 10
    features.append(seq_max_upload_1)               #sequencia maxima de uploads aceita um "-1" a meio (5) 10
    
    features.append(max_download_t)                 #quantidade maxima de download no mesmo instante (1 e 5) 10
    features.append(max_upload_t)                   #quantidade maxima de uploads no mesmo instante (1 e 5) 10
    features.append(count_download)                 #numero de quantidades de downloads no mesmo instante (1 e 5) 10
    features.append(count_upload)                   #numero de quantidades de uploads no mesmo instante (1 e 5) 10
    features.append(count_max_d)                    #quantidade de sequencias de downloads(conjuntos de -1 consecutivos)(1 e 5) 10
    features.append(count_max_u)                    #quantidade de sequencias de uploads (conjuntos de 1 consecutivos) (1 e 5) 10
    
    #features.append(count_download / tempo )        #frequencia com que existem downloads no mesmo instante
    #features.append(count_upload / tempo)           #frequencia com que existem uploads no mesmo instante
    #features.append(count_max_d/ tempo)             #frequencia com que existem downloads
    #features.append(count_max_u / tempo)            #frequencia com que existem uploads
    
    features.append(tempo_total_download)           #somatorio do tempo decorrido durante as sequencias de 1 encontradas (5) 10
    features.append((tempo_total_download/tempo)*100)       #percentagem de tempo durante o qual se realizou downloads(5) 10
    #features.append(max_download)                   #maior tempo com -1 consecutivos (5)
    
    features.append(counter_tempo_repetido)         #quantidade de instancias com tempos repetidos(5) 10
    #features.append(seq_time)                       #maior sequencia de tempos repetidos
    #features.append(max_ack)                        #numero total de syn/acks (-1,1,-1) 
    
    #features.append(total)                          #quantidade de pedidos por unidade de tempo (pedidos por segundo)
    #features.append(tempo)                          #tempo total de captura
    
    #features.append(media_d_u)                      #media de tempo entre um upload e download consecutivos
    #features.append(media_u)                        #media do intervalo de tempo entre cada upload
    #features.append(media_d)                        #media do intervalo de tempo entre cada download

    features.append(media_tempo_burst_d)            #media de tempo de duracao de um burst (5) 10
    features.append(media_tempo_burst_u)            #media de tempo de duracao de um burst (5) 10

    features.append(first_3_bursts_size_d)          #tamanho dos 3 primeiros bursts download 10
    features.append(first_3_bursts_size_u)          #tamanho dos 3 primeiros bursts upload 10

    features.append(counter_primeiros_d)            #quantidade de downloads das primeiras 100 capturas 10
    features.append(counter_primeiros_u)            #quantidade de uploads das primeiras 100 capturas 10
    
    #features.append(downloadPercent_200)           #percentagem de downloads realizados nos primeiros 200 pedidos
    #features.append(downloadSec_200)              #percentagem de downloads realizados nos primeiros 200 pedidos



    return features


def impute_missing(x):
        """Accepts a list of features containing 'X' in
        place of missing values. Consistently with the code
        by Cai et al, replaces 'X' with -1.
        """
        for i in range(len(x)):
            if x[i] == 'X':
                x[i] = -1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract feature vectors')
    parser.add_argument('--traces', type=str, help='Original traces directory.',
                        required=True)
    parser.add_argument('--out', type=str, help='Output directory for features.',
                        required=True)
    args = parser.parse_args()

    if not os.path.isdir(args.out):
        os.makedirs(args.out)

    #this takes quite a while
    print "Gathering features for monitored sites..."
    for site in range(0, 100):
        print site
        for instance in range(0, 90):
            fname = str(site) + "-" + str(instance)
            #Set up times, sizes
            f = open(args.traces + "/" + fname, "r")
            times = []
            sizes = []
            for x in f:
                x = x.split("\t")
                times.append(float(x[0]))
                sizes.append(int(x[1]))
            f.close()
    
            #Extract features. All features are non-negative numbers or X. 
            features = extract(times, sizes)

            #Replace X by -1 (Cai et al.)
            impute_missing(features)

            fout = open(args.out + "/" + fname + ".features", "w")
            for x in features[:-1]:
                fout.write(repr(x) + ",")
            fout.write(repr(features[-1]))
            fout.close()

    print "Finished gathering features for monitored sites."

    print "Gathering features for non-monitored sites..."
    #open world
    for site in range(0, 9000):
        print site
        fname = str(site)
        #Set up times, sizes
        f = open(args.traces + "/" + fname, "r")
        times = []
        sizes = []
        for x in f:
            x = x.split("\t")
            times.append(float(x[0]))
            sizes.append(int(x[1]))
        f.close()
    
        #Extract features. All features are non-negative numbers or X. 
        features = extract(times, sizes)

        #Replace X by -1 (Cai et al.)
        impute_missing(features)

        fout = open(args.out + "/" + fname + ".features", "w")
        for x in features[:-1]:
            fout.write(repr(x) + ",")
        fout.write(repr(features[-1]))
        fout.close()

    print "Finished gathering features for non-monitored sites."
    f.close()
