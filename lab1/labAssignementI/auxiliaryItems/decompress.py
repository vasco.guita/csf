#!/usr/bin/python2.7 

import sys, struct, numpy, PIL as pillow
from PIL import Image

def compose(v):
	data = ""
	vString = "".join(str(e) for e in v)
	size = getsize(vString[:32]) * 8
	vString = vString[32:]
	vString = vString[:size]
	while vString != "" :
		data += str(chr(int(vString[:8], 2)))
		vString = vString[8:]

	return data

def getsize(v):
	size = 0
	for i in range(0, 4, 1):
		size += pow(256, i)*int(v[:8], 2)
		v = v[8:]
	return size

def get_bit(n,i):
	mask = 1 << i
	n &= mask
	if (n == 0) :
		return 0
	else :
		return 1

def deembed(imgFile, password):
	img = Image.open(imgFile)
	width, height = img.size
	conv = img.convert('RGBA').getdata()
	displacement = 0
	v = []
	for h in range(height):
		for w in range(width):
			if displacement < password:
				displacement = displacement + 1
				continue
			r, g, b, a = conv.getpixel((w, h))
			v.append(get_bit(r, 0))
			v.append(get_bit(r, 1))
			v.append(get_bit(g, 0))
			v.append(get_bit(g, 1))
			v.append(get_bit(b, 0))
			v.append(get_bit(b, 1))
	return compose(v)

if __name__ == '__main__':
	password = int(sys.argv[2]) % 13 if len(sys.argv) > 2 else 0
	print deembed(sys.argv[1], password)