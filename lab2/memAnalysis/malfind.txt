Process: hud-service Pid: 1693 Address: 0x7f7f19f0c000 File: Anonymous Mapping
Protection: VM_READ|VM_WRITE|VM_EXEC
Flags: VM_READ|VM_WRITE|VM_EXEC|VM_MAYREAD|VM_MAYWRITE|VM_MAYEXEC|VM_ACCOUNT|VM_CAN_NONLINEAR

0x007f7f19f0c000  98 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0x007f7f19f0c010  53 41 57 41 56 41 55 55 48 8b df 48 83 ec 60 48   SAWAVAUUH..H..`H
0x007f7f19f0c020  8b 43 10 48 83 e8 02 48 89 44 24 40 48 89 dd 48   .C.H...H.D$@H..H
0x007f7f19f0c030  89 d8 48 8b 58 08 4c 8b 78 18 48 8b 08 8b 40 40   ..H.X.L.x.H...@@

0x7f7f19f0c000 98               CWDE
0x7f7f19f0c001 0300             ADD EAX, [RAX]
0x7f7f19f0c003 0000             ADD [RAX], AL
0x7f7f19f0c005 0000             ADD [RAX], AL
0x7f7f19f0c007 0000             ADD [RAX], AL
0x7f7f19f0c009 0000             ADD [RAX], AL
0x7f7f19f0c00b 0000             ADD [RAX], AL
0x7f7f19f0c00d 0000             ADD [RAX], AL
0x7f7f19f0c00f 005341           ADD [RBX+0x41], DL
0x7f7f19f0c012 57               PUSH RDI
0x7f7f19f0c013 4156             PUSH R14
0x7f7f19f0c015 4155             PUSH R13
0x7f7f19f0c017 55               PUSH RBP
0x7f7f19f0c018 488bdf           MOV RBX, RDI
0x7f7f19f0c01b 4883ec60         SUB RSP, 0x60
0x7f7f19f0c01f 488b4310         MOV RAX, [RBX+0x10]
0x7f7f19f0c023 4883e802         SUB RAX, 0x2
0x7f7f19f0c027 4889442440       MOV [RSP+0x40], RAX
0x7f7f19f0c02c 4889dd           MOV RBP, RBX
0x7f7f19f0c02f 4889d8           MOV RAX, RBX
0x7f7f19f0c032 488b5808         MOV RBX, [RAX+0x8]
0x7f7f19f0c036 4c8b7818         MOV R15, [RAX+0x18]
0x7f7f19f0c03a 488b08           MOV RCX, [RAX]
0x7f7f19f0c03d 8b4040           MOV EAX, [RAX+0x40]

Process: compiz Pid: 1877 Address: 0x7fce1da3c000 File: Anonymous Mapping
Protection: VM_READ|VM_WRITE|VM_EXEC
Flags: VM_READ|VM_WRITE|VM_EXEC|VM_MAYREAD|VM_MAYWRITE|VM_MAYEXEC|VM_ACCOUNT|VM_CAN_NONLINEAR

0x007fce1da3c000  53 55 8b ea 49 8b d9 33 c0 3b e8 0f 84 4d 00 00   SU..I..3.;...M..
0x007fce1da3c010  00 8b c6 3b 87 78 04 00 00 0f 43 87 78 04 00 00   ...;.x....C.x...
0x007fce1da3c020  8b 97 70 04 00 00 48 0f af c2 48 03 87 68 04 00   ..p...H...H..h..
0x007fce1da3c030  00 3b e8 48 8b f0 f3 0f 6f 06 f3 0f 7f 03 f3 0f   .;.H....o.......

0x7fce1da3c000 53               PUSH RBX
0x7fce1da3c001 55               PUSH RBP
0x7fce1da3c002 8bea             MOV EBP, EDX
0x7fce1da3c004 498bd9           MOV RBX, R9
0x7fce1da3c007 33c0             XOR EAX, EAX
0x7fce1da3c009 3be8             CMP EBP, EAX
0x7fce1da3c00b 0f844d000000     JZ 0x7fce1da3c05e
0x7fce1da3c011 8bc6             MOV EAX, ESI
0x7fce1da3c013 3b8778040000     CMP EAX, [RDI+0x478]
0x7fce1da3c019 0f438778040000   CMOVAE EAX, [RDI+0x478]
0x7fce1da3c020 8b9770040000     MOV EDX, [RDI+0x470]
0x7fce1da3c026 480fafc2         IMUL RAX, RDX
0x7fce1da3c02a 48038768040000   ADD RAX, [RDI+0x468]
0x7fce1da3c031 3be8             CMP EBP, EAX
0x7fce1da3c033 488bf0           MOV RSI, RAX
0x7fce1da3c036 f30f6f06         MOVDQU XMM0, [RSI]
0x7fce1da3c03a f30f7f03         MOVDQU [RBX], XMM0
0x7fce1da3c03e f3               DB 0xf3
0x7fce1da3c03f 0f               DB 0xf

Process: compiz Pid: 1877 Address: 0x7fce48004000 File: Anonymous Mapping
Protection: VM_READ|VM_EXEC
Flags: VM_READ|VM_EXEC|VM_MAYREAD|VM_MAYWRITE|VM_MAYEXEC|VM_ACCOUNT|VM_CAN_NONLINEAR

0x007fce48004000  00 00 00 00 00 00 80 3f 00 00 00 00 00 00 80 3f   .......?.......?
0x007fce48004010  00 00 00 40 00 00 40 40 00 00 00 40 00 00 40 40   ...@..@@...@..@@
0x007fce48004020  00 00 00 00 00 00 00 00 00 00 80 3f 00 00 80 3f   ...........?...?
0x007fce48004030  00 00 00 00 00 00 00 00 00 00 80 3f 00 00 80 3f   ...........?...?

0x7fce48004000 0000             ADD [RAX], AL
0x7fce48004002 0000             ADD [RAX], AL
0x7fce48004004 0000             ADD [RAX], AL
0x7fce48004006 803f00           CMP BYTE [RDI], 0x0
0x7fce48004009 0000             ADD [RAX], AL
0x7fce4800400b 0000             ADD [RAX], AL
0x7fce4800400d 00803f000000     ADD [RAX+0x3f], AL
0x7fce48004013 400000           ADD [RAX], AL
0x7fce48004016 40400000         ADD [RAX], AL
0x7fce4800401a 004000           ADD [RAX+0x0], AL
0x7fce4800401d 004040           ADD [RAX+0x40], AL
0x7fce48004020 0000             ADD [RAX], AL
0x7fce48004022 0000             ADD [RAX], AL
0x7fce48004024 0000             ADD [RAX], AL
0x7fce48004026 0000             ADD [RAX], AL
0x7fce48004028 0000             ADD [RAX], AL
0x7fce4800402a 803f00           CMP BYTE [RDI], 0x0
0x7fce4800402d 00803f000000     ADD [RAX+0x3f], AL
0x7fce48004033 0000             ADD [RAX], AL
0x7fce48004035 0000             ADD [RAX], AL
0x7fce48004037 0000             ADD [RAX], AL
0x7fce48004039 00803f000080     ADD [RAX-0x7fffffc1], AL
0x7fce4800403f 3f               DB 0x3f

Process: compiz Pid: 1877 Address: 0x7fce4800e000 File: Anonymous Mapping
Protection: VM_READ|VM_EXEC
Flags: VM_READ|VM_EXEC|VM_MAYREAD|VM_MAYWRITE|VM_MAYEXEC|VM_ACCOUNT|VM_CAN_NONLINEAR

0x007fce4800e000  00 00 00 00 00 00 80 3f 00 00 00 00 00 00 80 3f   .......?.......?
0x007fce4800e010  00 00 00 40 00 00 40 40 00 00 00 40 00 00 40 40   ...@..@@...@..@@
0x007fce4800e020  00 00 00 00 00 00 00 00 00 00 80 3f 00 00 80 3f   ...........?...?
0x007fce4800e030  00 00 00 00 00 00 00 00 00 00 80 3f 00 00 80 3f   ...........?...?

0x7fce4800e000 0000             ADD [RAX], AL
0x7fce4800e002 0000             ADD [RAX], AL
0x7fce4800e004 0000             ADD [RAX], AL
0x7fce4800e006 803f00           CMP BYTE [RDI], 0x0
0x7fce4800e009 0000             ADD [RAX], AL
0x7fce4800e00b 0000             ADD [RAX], AL
0x7fce4800e00d 00803f000000     ADD [RAX+0x3f], AL
0x7fce4800e013 400000           ADD [RAX], AL
0x7fce4800e016 40400000         ADD [RAX], AL
0x7fce4800e01a 004000           ADD [RAX+0x0], AL
0x7fce4800e01d 004040           ADD [RAX+0x40], AL
0x7fce4800e020 0000             ADD [RAX], AL
0x7fce4800e022 0000             ADD [RAX], AL
0x7fce4800e024 0000             ADD [RAX], AL
0x7fce4800e026 0000             ADD [RAX], AL
0x7fce4800e028 0000             ADD [RAX], AL
0x7fce4800e02a 803f00           CMP BYTE [RDI], 0x0
0x7fce4800e02d 00803f000000     ADD [RAX+0x3f], AL
0x7fce4800e033 0000             ADD [RAX], AL
0x7fce4800e035 0000             ADD [RAX], AL
0x7fce4800e037 0000             ADD [RAX], AL
0x7fce4800e039 00803f000080     ADD [RAX-0x7fffffc1], AL
0x7fce4800e03f 3f               DB 0x3f

Process: compiz Pid: 1877 Address: 0x7fce4801c000 File: Anonymous Mapping
Protection: VM_READ|VM_EXEC
Flags: VM_READ|VM_EXEC|VM_MAYREAD|VM_MAYWRITE|VM_MAYEXEC|VM_ACCOUNT|VM_CAN_NONLINEAR

0x007fce4801c000  00 00 00 00 01 00 00 00 02 00 00 00 03 00 00 00   ................
0x007fce4801c010  04 00 00 00 05 00 00 00 06 00 00 00 07 00 00 00   ................
0x007fce4801c020  00 00 00 80 01 00 00 00 02 00 00 00 04 00 00 00   ................
0x007fce4801c030  08 00 00 00 10 00 00 00 20 00 00 00 00 00 80 3f   ...............?

0x7fce4801c000 0000             ADD [RAX], AL
0x7fce4801c002 0000             ADD [RAX], AL
0x7fce4801c004 0100             ADD [RAX], EAX
0x7fce4801c006 0000             ADD [RAX], AL
0x7fce4801c008 0200             ADD AL, [RAX]
0x7fce4801c00a 0000             ADD [RAX], AL
0x7fce4801c00c 0300             ADD EAX, [RAX]
0x7fce4801c00e 0000             ADD [RAX], AL
0x7fce4801c010 0400             ADD AL, 0x0
0x7fce4801c012 0000             ADD [RAX], AL
0x7fce4801c014 0500000006       ADD EAX, 0x6000000
0x7fce4801c019 0000             ADD [RAX], AL
0x7fce4801c01b 0007             ADD [RDI], AL
0x7fce4801c01d 0000             ADD [RAX], AL
0x7fce4801c01f 0000             ADD [RAX], AL
0x7fce4801c021 0000             ADD [RAX], AL
0x7fce4801c023 800100           ADD BYTE [RCX], 0x0
0x7fce4801c026 0000             ADD [RAX], AL
0x7fce4801c028 0200             ADD AL, [RAX]
0x7fce4801c02a 0000             ADD [RAX], AL
0x7fce4801c02c 0400             ADD AL, 0x0
0x7fce4801c02e 0000             ADD [RAX], AL
0x7fce4801c030 0800             OR [RAX], AL
0x7fce4801c032 0000             ADD [RAX], AL
0x7fce4801c034 1000             ADC [RAX], AL
0x7fce4801c036 0000             ADD [RAX], AL
0x7fce4801c038 2000             AND [RAX], AL
0x7fce4801c03a 0000             ADD [RAX], AL
0x7fce4801c03c 0000             ADD [RAX], AL
0x7fce4801c03e 80               DB 0x80
0x7fce4801c03f 3f               DB 0x3f

Process: compiz Pid: 1877 Address: 0x7fce50b45000 File: Anonymous Mapping
Protection: VM_READ|VM_EXEC
Flags: VM_READ|VM_EXEC|VM_MAYREAD|VM_MAYWRITE|VM_MAYEXEC|VM_ACCOUNT|VM_CAN_NONLINEAR

0x007fce50b45000  55 48 89 e5 c5 f8 28 07 c5 f8 28 0a 48 b8 00 a0   UH....(...(.H...
0x007fce50b45010  04 48 ce 7f 00 00 c4 e2 79 18 10 c5 f8 58 d2 c5   .H......y....X..
0x007fce50b45020  f8 5c 1e c5 f0 5c c8 c4 e3 79 04 e1 e1 c5 e0 59   .\...\...y.....Y
0x007fce50b45030  e4 c5 fa 16 ec c5 da 5c e5 48 b8 04 a0 04 48 ce   .......\.H....H.

0x7fce50b45000 55               PUSH RBP
0x7fce50b45001 4889e5           MOV RBP, RSP
0x7fce50b45004 c5f82807         VMOVAPS XMM0, [RDI]
0x7fce50b45008 c5f8280a         VMOVAPS XMM1, [RDX]
0x7fce50b4500c 48b800a00448ce7f0000 MOV RAX, 0x7fce4804a000
0x7fce50b45016 c4e2791810       VBROADCASTSS XMM2, [RAX]
0x7fce50b4501b c5f858d2         VADDPS XMM2, XMM0, XMM2
0x7fce50b4501f c5f85c1e         VSUBPS XMM3, XMM0, DQWORD [RSI]
0x7fce50b45023 c5f05cc8         VSUBPS XMM1, XMM1, XMM0
0x7fce50b45027 c4e37904e1e1     VPERMILPS XMM4, XMM1, 0xe1
0x7fce50b4502d c5e059e4         VMULPS XMM4, XMM3, XMM4
0x7fce50b45031 c5fa16ec         VMOVSHDUP XMM5, XMM4
0x7fce50b45035 c5da5ce5         VSUBSS XMM4, XMM4, XMM5
0x7fce50b45039 48               DB 0x48
0x7fce50b4503a b8               DB 0xb8
0x7fce50b4503b 04a0             ADD AL, 0xa0
0x7fce50b4503d 0448             ADD AL, 0x48
0x7fce50b4503f ce               DB 0xce

Process: compiz Pid: 1877 Address: 0x7fce50b54000 File: Anonymous Mapping
Protection: VM_READ|VM_WRITE|VM_EXEC
Flags: VM_READ|VM_WRITE|VM_EXEC|VM_MAYREAD|VM_MAYWRITE|VM_MAYEXEC|VM_ACCOUNT|VM_CAN_NONLINEAR

0x007fce50b54000  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0x007fce50b54010  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0x007fce50b54020  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
0x007fce50b54030  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................

0x7fce50b54000 0000             ADD [RAX], AL
0x7fce50b54002 0000             ADD [RAX], AL
0x7fce50b54004 0000             ADD [RAX], AL
0x7fce50b54006 0000             ADD [RAX], AL
0x7fce50b54008 0000             ADD [RAX], AL
0x7fce50b5400a 0000             ADD [RAX], AL
0x7fce50b5400c 0000             ADD [RAX], AL
0x7fce50b5400e 0000             ADD [RAX], AL
0x7fce50b54010 0000             ADD [RAX], AL
0x7fce50b54012 0000             ADD [RAX], AL
0x7fce50b54014 0000             ADD [RAX], AL
0x7fce50b54016 0000             ADD [RAX], AL
0x7fce50b54018 0000             ADD [RAX], AL
0x7fce50b5401a 0000             ADD [RAX], AL
0x7fce50b5401c 0000             ADD [RAX], AL
0x7fce50b5401e 0000             ADD [RAX], AL
0x7fce50b54020 0000             ADD [RAX], AL
0x7fce50b54022 0000             ADD [RAX], AL
0x7fce50b54024 0000             ADD [RAX], AL
0x7fce50b54026 0000             ADD [RAX], AL
0x7fce50b54028 0000             ADD [RAX], AL
0x7fce50b5402a 0000             ADD [RAX], AL
0x7fce50b5402c 0000             ADD [RAX], AL
0x7fce50b5402e 0000             ADD [RAX], AL
0x7fce50b54030 0000             ADD [RAX], AL
0x7fce50b54032 0000             ADD [RAX], AL
0x7fce50b54034 0000             ADD [RAX], AL
0x7fce50b54036 0000             ADD [RAX], AL
0x7fce50b54038 0000             ADD [RAX], AL
0x7fce50b5403a 0000             ADD [RAX], AL
0x7fce50b5403c 0000             ADD [RAX], AL
0x7fce50b5403e 0000             ADD [RAX], AL

Process: main Pid: 14921 Address: 0x7f128817a000 File: Anonymous Mapping
Protection: VM_READ|VM_WRITE|VM_EXEC
Flags: VM_READ|VM_WRITE|VM_EXEC|VM_MAYREAD|VM_MAYWRITE|VM_MAYEXEC|VM_ACCOUNT|VM_CAN_NONLINEAR

0x007f128817a000  00 00 00 00 00 00 00 00 b9 0f 00 00 00 00 00 00   ................
0x007f128817a010  49 bb 62 3f cd 85 12 7f 00 00 49 ba 10 a0 17 88   I.b?......I.....
0x007f128817a020  12 7f 00 00 f8 49 ff e3 f8 dc 0e 88 12 7f 00 00   .....I..........
0x007f128817a030  c0 46 ee 85 12 7f 00 00 d0 dc 0e 88 12 7f 00 00   .F..............

0x7f128817a000 0000             ADD [RAX], AL
0x7f128817a002 0000             ADD [RAX], AL
0x7f128817a004 0000             ADD [RAX], AL
0x7f128817a006 0000             ADD [RAX], AL
0x7f128817a008 b90f000000       MOV ECX, 0xf
0x7f128817a00d 0000             ADD [RAX], AL
0x7f128817a00f 0049bb           ADD [RCX-0x45], CL
0x7f128817a012 62               DB 0x62
0x7f128817a013 3f               DB 0x3f
0x7f128817a014 cd85             INT 0x85
0x7f128817a016 127f00           ADC BH, [RDI+0x0]
0x7f128817a019 0049ba           ADD [RCX-0x46], CL
0x7f128817a01c 10a01788127f     ADC [RAX+0x7f128817], AH
0x7f128817a022 0000             ADD [RAX], AL
0x7f128817a024 f8               CLC
0x7f128817a025 49ffe3           JMP R11
0x7f128817a028 f8               CLC
0x7f128817a029 dc0e             FMUL QWORD [RSI]
0x7f128817a02b 8812             MOV [RDX], DL
0x7f128817a02d 7f00             JG 0x7f128817a02f
0x7f128817a02f 00c0             ADD AL, AL
0x7f128817a031 46ee             OUT DX, AL
0x7f128817a033 8512             TEST [RDX], EDX
0x7f128817a035 7f00             JG 0x7f128817a037
0x7f128817a037 00d0             ADD AL, DL
0x7f128817a039 dc0e             FMUL QWORD [RSI]
0x7f128817a03b 8812             MOV [RDX], DL
0x7f128817a03d 7f00             JG 0x7f128817a03f
0x7f128817a03f 00               DB 0x0

