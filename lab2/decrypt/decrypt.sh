#!/bin/bash

mkdir mnt
sudo mount -o loop,offset=1048576 $1 mnt

mkdir decrypted

for f in $(find mnt/home/sally/Documents/ -name "*.encrypted")
do
	python aes-ctr.py -d -i $f -o decrypted/$(echo $f | sed -r "s/.+\/(.+)\..+/\1/") -k 47683b9a9663c065353437b35c5d8519 -iv $(echo $(head -c 16 $f | od -A n -t x1) | tr -d ' ')
done

sudo umount mnt/
rmdir mnt
